import Vue from "vue";
import App from "./App.vue";
import VueRouter from "vue-router";
import Home from "@/components/Home";
import CrearGrupo from "@/components/CrearGrupo";
import TiraDeMaterias from "@/components/TiraDeMaterias";
import CrearMateria from "@/components/CrearMateria";
import InscribirAlumno from "@/components/InscribirAlumno";
import Asistencia from "@/components/Asistencia";
import RegistroDeCalificaciones from "@/components/RegistroDeCalificaciones";
import ReporteGrupos from "@/components/ReporteGrupos";
import ReporteInscripciones from "@/components/ReporteInscripciones";
import ReporteGruposMaestro from "@/components/ReporteGruposMaestro";
import ListaAlumnosProfesor from "@/components/ListaAlumnosProfesor";
import ReporteAsistencia from "@/components/ReporteAsistencia";
import "@/assets/global.css";

Vue.config.productionTip = false;

const routes = [
  { path: "/", component: Home },
  { path: "/crear-grupo", component: CrearGrupo },
  { path: "/tira-de-materias", component: TiraDeMaterias },
  { path: "/crear-materia", component: CrearMateria },
  { path: "/inscribir-alumno", component: InscribirAlumno },
  { path: "/asistencia", component: Asistencia },
  { path: "/registro-calificaciones", component: RegistroDeCalificaciones },
  { path: "/reporte-grupos", component: ReporteGrupos },
  { path: "/reporte-inscripciones", component: ReporteInscripciones },
  { path: "/reporte-grupos-profesor", component: ReporteGruposMaestro },
  { path: "/lista-alumnos-profesor", component: ListaAlumnosProfesor },
  { path: "/reporte-asistencia-profesor", component: ReporteAsistencia }
];
const router = new VueRouter({ routes });

Vue.use(VueRouter);

new Vue({
  router,
  data: { userType: null, userFirstName: null, userData: {} },
  render: h => h(App)
}).$mount("#app");
