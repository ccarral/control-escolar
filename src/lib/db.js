const credentials = require("../../.credentials.json");
const mysql = require("mysql");

const conn = mysql.createConnection(credentials.mysql);

conn.connect();

exports.dbQuery = (querystr, values, callback) => {
  console.log(mysql.format(querystr, values));
  conn.query(querystr, values, callback);
};
