const express = require("express");
const bodyParser = require("body-parser");
const { dbQuery } = require("./src/lib/db");

const app = express();

app.set("port", process.env.PORT || 3000);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use((req, res, next) => {
  res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  res.setHeader("Pragma", "no-cache");
  res.setHeader("Expires", "0");
  next();
});

app.post("/api/query", (req, res) => {
  const query = req.body.query;
  const values = req.body.values;
  console.log(req.body);

  dbQuery(query, values, (err, results, fields) => {
    if (err) {
      console.log(err);
      res.json({ correct: false, results: [], fields: [] });
    } else {
      console.log(results);
      res.json({ correct: true, results, fields });
    }
    console.log("Se hizo la consulta a la bd");
  });
});

app.listen(app.get("port"), () => {
  console.log(`Servidor escuchando en: http://localhost:${app.get("port")}/`); // eslint-disable-line no-console
});
